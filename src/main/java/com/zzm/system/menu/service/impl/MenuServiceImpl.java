package com.zzm.system.menu.service.impl;

import com.zzm.system.menu.mapper.MenuMapper;
import com.zzm.system.menu.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;
    @Override
    public Set<String> selectPermsByUserId(Long userId) {
        List<String> perms = menuMapper.selectPermsByUserId(userId);
        Set<String> permset = new HashSet<>();

        for(String perm : perms){
            permset.addAll(Arrays.asList(perm.trim().split(",")));
        }
        return permset;
    }
}
