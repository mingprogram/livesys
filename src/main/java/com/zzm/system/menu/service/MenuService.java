package com.zzm.system.menu.service;

import java.util.Set;

public interface MenuService {

    /**
     * 根据用户ID查询权限
     * @param userId
     * @return
     */
    public Set<String> selectPermsByUserId(Long userId);
}
