package com.zzm.system.role.mapper;

import com.zzm.system.role.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * 角色表 数据层
 * 
 * @author ZZM
 */

@MapperScan
public interface RoleMapper
{

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<Role> selectRolesByUserId(@Param("userId") Long userId);

}
