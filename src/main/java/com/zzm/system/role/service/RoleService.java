package com.zzm.system.role.service;

import java.util.Set;

public interface RoleService {
    public Set<String> selectRoles(Long userid);
}
