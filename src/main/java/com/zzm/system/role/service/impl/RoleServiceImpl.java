package com.zzm.system.role.service.impl;

import com.zzm.system.role.entity.Role;
import com.zzm.system.role.mapper.RoleMapper;
import com.zzm.system.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Set<String> selectRoles(Long userid) {
        List<Role> roles = roleMapper.selectRolesByUserId(userid);
        Set<String> perms = new HashSet<>();

        for(Role role : roles){
            perms.addAll(Arrays.asList(role.getRoleKey().trim().split(",")));
        }
        return perms;
    }
}
