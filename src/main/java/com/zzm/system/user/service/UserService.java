package com.zzm.system.user.service;

import com.zzm.system.user.entity.User;

public interface UserService {

    /**
     * 通过用户名查找用户
     * @param loginname
     * @return
     */
    public User selectUserByLoginName(String loginname);

    public User selectUserByLoginNameAndPassword(String loginname, String password);


}
