package com.zzm.system.user.service.impl;

import com.zzm.system.user.entity.User;
import com.zzm.system.user.mapper.UserMapper;
import com.zzm.system.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userDao;


    @Override
    public User selectUserByLoginName(String loginname) {
        return userDao.selectUserByLoginName(loginname);
    }

    @Override
    public User selectUserByLoginNameAndPassword(String loginname, String password) {
        return userDao.selectUserByLoginNameAndPassword(loginname,password);
    }
}
