package com.zzm.system.user.controller;

import com.zzm.common.utils.AjaxResult;
import com.zzm.system.user.entity.User;
import com.zzm.system.user.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


@Controller
public class LoginController{

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @GetMapping("/main")
    public String main(){
        return "main";
    }

    @RequestMapping("/date")
    public String date(Date date){

        System.out.println(date);
        return "login";
    }

    @InitBinder
    public void initBinder(ServletRequestDataBinder binder){
        binder.registerCustomEditor(Date.class,new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"),true));
    }


    @RequestMapping("/redirect")
    public String redirect(){
        return "redirect:hello";
    }

    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public String upload(HttpServletRequest request) throws Exception {
        MultipartHttpServletRequest mhsr = (MultipartHttpServletRequest)request;
        MultipartFile file = mhsr.getFile("file");
        String filename = file.getOriginalFilename();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        FileOutputStream fos = new FileOutputStream(request.getSession().getServletContext().getRealPath("/")+"upload/"+sdf.format(new Date())+filename.substring(filename.indexOf(".")));
        fos.write(file.getBytes());
        fos.flush();
        fos.close();
        return "hello";
    }

    /*@RequestMapping("/loginForm")
    public String loginForm(HttpServletRequest request){
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = userService.getUser(username,password);
        if(user != null){
            return "main";
        }
        return "login";
    }*/
/*
    @RequestMapping(value = "/loginForm",method = RequestMethod.POST)
    public String loginForm(String username,String password){
        User user = userService.getUser(username,password);
        if(user != null){
            return "main";
        }
        return "login";
    }*/

    @RequestMapping(value = "/login",method = {RequestMethod.POST})
    @ResponseBody
    public AjaxResult loginForm(@RequestBody User user){
        String username = user.getLoginName();
        String password = user.getPassword();

        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        Subject subject = SecurityUtils.getSubject();

        try {
            subject.login(token);
            return AjaxResult.success(0,"操作成功");
        } catch (AuthenticationException e) {

            String errormessage = e.getMessage();
            String msg = "用户或密码错误";
            if(e.getMessage() != null && !e.getMessage().equals("")){
                msg = e.getMessage();
            }

            return AjaxResult.error(500,msg);
        }
        /*User getUser = userService.getUser(username,password);
        String result = null;
        if(getUser == null){
            result = AjaxResultUtil.retContent(1012,null);

        }else{
            result = AjaxResultUtil.retContent(1016,null);
        }*/

    }
}
