package com.zzm.system.user.mapper;

import com.zzm.system.user.entity.User;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

@MapperScan
public interface UserMapper {

    /**
     * 通过用户名查找用户
     * @param loginname
     * @return
     */
    public User selectUserByLoginName(String loginname);

    public User selectUserByLoginNameAndPassword(@Param("loginname") String loginname, @Param("password") String password);
}
