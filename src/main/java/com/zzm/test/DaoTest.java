package com.zzm.test;

import com.zzm.common.utils.Md5Utils;
import com.zzm.system.user.entity.User;
import com.zzm.system.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class DaoTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserService userService;
    @org.junit.jupiter.api.Test
    public void jdbcTest(){

        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from t_user");

        for(Map<String,Object> map : list){
            String username = (String)map.get("USERNAME");
            System.out.println(username);
        }
    }

    @Test
    public void userServiceTest(){

        //UserService userService = SpringUtils.getBean("userService");
        User user = userService.selectUserByLoginName("admin");
        System.out.println(user.getUserName()+":"+user.getPassword());
    }

    @Test
    public void userServiceTest1(){

        //UserService userService = SpringUtils.getBean("userService");
        String password = Md5Utils.encript("admin","admin",111111);

        User user = userService.selectUserByLoginNameAndPassword("admin",password);
        System.out.println(user.getUserName()+":"+user.getPassword());
    }

}
