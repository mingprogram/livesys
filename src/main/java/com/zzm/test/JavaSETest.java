package com.zzm.test;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;

public class JavaSETest {
    public static void main(String[] args) {
        SecureRandomNumberGenerator secureRandom = new SecureRandomNumberGenerator();
        String hex = secureRandom.nextBytes(3).toHex();
        System.out.println(hex);
    }
}
