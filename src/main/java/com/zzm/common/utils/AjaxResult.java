package com.zzm.common.utils;


import java.util.HashMap;

public  class AjaxResult extends HashMap<String,Object> {

    private static final long serialVersionUID = 1L;

    public AjaxResult(){

    }

    /**
     *
     * @param code 错误码
     * @param msg 错误信息
     * @return
     */
    public static AjaxResult error(int code, String msg){
        AjaxResult json = new AjaxResult();
        json.put("code",code);
        json.put("msg",msg);

        return json;
    }

    /**
     *
     * @param code 成功码
     * @param msg 成功信息
     * @return
     */
    public static AjaxResult success(int code, String msg){
        AjaxResult json = new AjaxResult();
        json.put("code",code);
        json.put("msg",msg);

        return json;
    }
}
