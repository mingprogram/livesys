package com.zzm.common.utils;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

public class OutputJson implements Serializable {
    private static final long serialVersionUID = 1L;

    private int status;
    private String message;
    private Object data;

    public OutputJson(int status,String message,Object data){
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {

        return JSON.toJSONString(this);
    }
}
