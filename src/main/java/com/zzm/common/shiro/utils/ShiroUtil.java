package com.zzm.common.shiro.utils;

import com.zzm.system.user.entity.User;
import org.apache.shiro.SecurityUtils;

public class ShiroUtil {
    public static User getUser(){
        User user = (User)SecurityUtils.getSubject().getPrincipal();
        return user;
    }

    public static Long getUserId(){
        return  getUser().getUserId();
    }
}
