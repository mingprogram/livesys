package com.zzm.common.shiro.filter;

import org.apache.shiro.web.servlet.AdviceFilter;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShiroUserFilter extends AdviceFilter {
    protected boolean preHandler(HttpServletRequest request, HttpServletResponse response){
        //User user = (User)request.getSession().getAttribute("user");
        String requestWith = request.getHeader("X-Request-With");
        if(!StringUtils.isEmpty(requestWith) && requestWith.equals("XMLHttpRequest")){
            return false;
        }

        return true;
    }
}
