<%--
  Created by IntelliJ IDEA.
  User: zzm
  Date: 2018/7/26
  Time: 17:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.*" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path;

%>
<html>
<head>
    <base href="<%=basePath %>"/>
    <title>用户列表</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/DataTables-1.10.18/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/DataTables-1.10.18/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/DataTables-1.10.18/css/dataTables.bootstrap.css">

    <script type="text/javascript" src="<%=basePath %>/static/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>/static/js/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="<%=basePath %>/static/js/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<%=basePath %>/static/js/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
        $(function () {

            $("#userlist").DataTable({
                "ajax":'<%=basePath %>/static/json/userlist.json',
                "columns": [
                    { "data": "name" },
                    { "data": "position" },
                    { "data": "office" },
                    { "data": "extn" },
                    { "data": "start_date" },
                    { "data": "salary" }
                ],
                language: {
                    url:'<%=basePath %>/static/js/DataTables-1.10.18/js/Chinese.json'
                }
            });
        });
    </script>
</head>

<body style="width: 98%">
<table id="userlist" class="display" width="100%">
    <thead>
    <tr>
        <th>Name</th>
        <th>Position</th>
        <th>Office</th>
        <th>Extn.</th>
        <th>Start date</th>
        <th>Salary</th>
    </tr>
    </thead>
</table>
</body>
</html>
