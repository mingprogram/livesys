<%--
  User: zzm
  Created by IntelliJ IDEA.
  Date: 2018/7/26
  Time: 17:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.*" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path;

%>
<html>
<head>
    <base href="<%=basePath %>"/>
    <title>登录页面</title>
    <script src="<%=basePath %>/static/js/jquery/jquery-3.3.1.min.js"></script>
    <script src="<%=basePath %>/static/js/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script type="text/javascript">
        function login(){

            var json = {
                'loginName':$(':input[name=loginName]').val(),
                'password':$(':input[name=password]').val()
            };
            //json字符串 {"username":"admin","password":"123456"}
            var postdata = JSON.stringify(json);//json对象转换json字符串
            $.ajax({
                type : 'POST',
                contentType : 'application/json;charset=utf-8',
                /**
                 *(默认: true) 默认情况下，通过data选项传递进来的数据，如果是一个对象(技术上讲只要不是字符串)，
                 * 都会处理转化成一个查询字符串，以配合默认内容类型 "application/x-www-form-urlencoded"。
                 * 如果要发送 DOM 树信息或其它不希望转换的信息，请设置为 false。
                 */
                processData : false,
                url : '<%=basePath%>/login',
                dataType : 'json',
                data : postdata,
                success : function(data) {
                    //alert(typeof data);
                    var status = data.code;
                    var message = data.msg;
                    //alert(status);
                    if(status == 500){
                        $('#errorlogin').html(message);
                    }else if(status == 0){
                        window.location.href = "<%=basePath%>/main";
                    }
                },
                error : function() {
                    alert('error...');
                }
            })
        }

    </script>
</head>

<body>

    <div class="container-fluid" style="margin-top:15%;margin-right:30%;margin-left: 30%;">
        <div class="row" style="height: 300px;">
            <form id="loginForm">
            <div class="input-group input-group-lg" style="padding: 20px">
                <span class="input-group-addon" ><span class="glyphicon glyphicon-user"></span></span>
                <input type="text" class="form-control" placeholder="loginName" id="loginName" name="loginName" value="${loginName}">

            </div>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="errorusername" style="color: red"></span>
            <div class="input-group input-group-lg" style="padding: 20px">
                <span class="input-group-addon" ><span class="glyphicon glyphicon-lock"></span></span>
                <input type="password" class="form-control" placeholder="Password" id="password" name="password" value="${password}">
            </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="errorpassword" style="color: red"></span>
            <div style="padding: 20px">
                <input type="button" class="form-control" value="登录" onclick="login()">
            </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="errorlogin" style="color: red"></span>
            </form>

        </div>
    </div>
    <div style="padding-top: 100px">
        dd
    </div>
</body>
</html>
