<%--
  Created by IntelliJ IDEA.
  User: zzm
  Date: 2018/7/26
  Time: 17:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.*" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path;

%>
<html>
<head>
    <base href="<%=basePath %>"/>
    <title>主页面</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/menujs/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath%>/static/js/Font-Awesome-3.2.1/css/font-awesome.min.css">
    <script type="text/javascript" src="<%=basePath %>/static/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>/static/js/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="<%=basePath %>/static/js/menujs/js/metisMenu.min.js" ></script>


    <script type="text/javascript">

        $(function () {
            $("#sysmenu").metisMenu();
        });

        function url(url) {
            //alert(url);
            $('#mainIframe').attr('src',url);
        }
    </script>

    <style type="text/css">

    </style>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2" style="background-color: #363636;height: 100%;">
            <div style="height: 10%">
                <h1 class="text-danger">实用系统</h1>
            </div>

            <div class="" style="height: 80%">
                <ul id="sysmenu" class="metismenu nav nav-pills nav-stacked">

                    <li>
                        <a href="#" style="background-color: #9d9d9d"><i class="icon-th-list"></i> 系统管理</a>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="javascript:void(0)" onclick="url('<%=basePath %>/user/list')"><i class="icon-user"></i> 用户管理</a></li>
                            <li><a href="#"><i class="icon-group"></i>角色管理</a></li>
                            <li><a href="#"><i class="icon-key"></i>权限管理</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
        <div class="col-md-10">
            <div style="height: 10%">
            </div>

            <div style=""><iframe id="mainIframe" src=""  width="100%" height="90%" frameborder="0"></iframe></div>
        </div>
    </div>
</div>
</body>
</html>
