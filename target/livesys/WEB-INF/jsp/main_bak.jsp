<%--
  Created by IntelliJ IDEA.
  User: zzm
  Date: 2018/7/26
  Time: 17:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.*" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path;

%>
<html>
<head>
    <base href="<%=basePath %>"/>
    <title>主页面</title>
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<%=basePath %>/static/js/bootstrap-table-1.12.1/bootstrap-table.css">

    <script type="text/javascript" src="<%=basePath %>/static/js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>/static/js/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>

    <script type="text/javascript" src="<%=basePath %>/static/js/bootstrap-table-1.12.1/bootstrap-table.js"></script>
    <script type="text/javascript" src="<%=basePath %>/static/js/bootstrap-table-1.12.1/locale/bootstrap-table-zh-CN.js"></script>



    <script type="text/javascript">
        $(function () {
            $('#table').bootstrapTable({
                url:'<%=basePath %>/static/json/data.json',
                //url: '/Home/GetDepartment',         //请求后台的URL（*）
                method: 'get',                      //请求方式（*）
                toolbar: '#toolbar',                //工具按钮用哪个容器
                striped: true,                      //是否显示行间隔色
                cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true,                   //是否显示分页（*）
                sortable: true,                     //是否启用排序
                sortOrder: "asc",                   //排序方式
                //queryParams: oTableInit.queryParams,//传递参数（*）
                sidePagination: "client",           //分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1,                       //初始化加载第一页，默认第一页
                pageSize: 3,                       //每页的记录行数（*）
                pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                strictSearch: true,
                showColumns: true,                  //是否显示所有的列
                showRefresh: true,                  //是否显示刷新按钮
                minimumCountColumns: 2,             //最少允许的列数
                clickToSelect: true,                //是否启用点击选中行
                height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                uniqueId: "ID",                     //每一行的唯一标识，一般为主键列
                showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
                cardView: false,                    //是否显示详细视图
                detailView: false,                   //是否显示父子表
                columns:[{
                    field:'id',
                    title:'序号'
                },{
                    field:'name',
                    title:'姓名'
                }]
            });
        });

    </script>
</head>

<body>
<table id="table"></table>

</body>
</html>
